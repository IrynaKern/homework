/*Создайте страницу с кнопкой, 
при нажатии по кнопке на странице создается div с произвольным текстом. 
После создания 10 дивов все они должны удалиться */

window.onload = () => {
    const button = document.querySelector("#new-div"), // создаём переменную, в которой будет хранится кнопка, найденная по id
    keepAllDiv = document.createElement("div"); // создаём переменную, в которой будут хранится все div, созданные кнопкой
    button.after(keepAllDiv); // добавляем div после button(кнопки)
    let counter = 0; // создаём переменную - счётчик со значением 0

    button.onclick = () => {  // создаём событие нажатия на кнопку
        if (counter < 10) { // условие на количество повторений
            counter += 1; // счетчик увеличивается на 1
            const createDiv = document.createElement("div"); // добавляем div на страницу
            createDiv.innerHTML = "Some text"; // добавляем текст в div
            keepAllDiv.appendChild(createDiv); // выводим новый div как дочерний элемент другого div
            return counter; // возращает значение счётчика
        } else {
            keepAllDiv.remove(); // удаляем созданные div
        }           
    } 
}       
