const div = document.querySelector("#root"), // создаём переменную и присваиваем ей значение - находим элемент (тег) по id "root"
h1 = document.createElement("h1"); // создаём переменную и присваиваем ей значение - создаём элемент (тег) h1
h1.innerHTML = "Курсы валют"; // добавляем текст в h1 между открывающим и закривающими тегами 
div.appendChild(h1); // добавляем элемент (тег) h1 в div

const arrCurrency = [ // создаем массив c валютами и их курсами)
   {currency: "USD", base_currency: "UAH", buy: 27.80000, sale: 28.50000},
   {currency: "EUR", base_currency: "UAH", buy: 30.55000, sale: 31.65000},
   {currency: "RUB", base_currency: "UAH", buy: 0.32000, sale: 0.36500},
   {currency: "BTC", base_currency: "USD", buy: 5982.4271, sale: 6612.1563}
]

table = document.createElement("table"); // создаём таблицу
createThead = () => { // создаём функцию, которая будет создавать thead и заполнять значениями
    const thead = document.createElement("thead"); // создаём thead
    trThead = document.createElement("tr"); // создаём строку thead
    for (let key in arrCurrency[0]) { // создаём функцию перебора свойств одного объекта
        const tdThead = document.createElement("td"); // создаём элемент (тег) td 
        tdThead.innerHTML = `${key}`; // добавляем текст в td
        trThead.appendChild(tdThead); // добавляем элемент (тег) td в tr
    }   
    thead.appendChild(trThead); //добавляем элемент (тег) tr в thead
    table.appendChild(thead); //добавляем элемент (тег) thead в table
}

createThead();    

arrCurrency.forEach((element) => { // создаём функцию перебора массива для каждого элемента
    tr = document.createElement("tr");// создаём переменную и присваиваем ей значение - создаём элемент (тег) tr
    for (let key in element) {// создаём функцию перебора свойств объекта
        const td = document.createElement("td"); //создаём переменную и присваиваем ей значение - создаём элемент (тег) td
        td.innerHTML = `${element[key]}`; // добавляем текст в td
        tr.appendChild(td); // добавляем элемент (тег) td в tr
    }
    table.appendChild(tr); // добавляем элемент (тег) tr в table
    div.appendChild(table); // добавляем элемент (тег) table в div
})
