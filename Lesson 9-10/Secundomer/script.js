/*Разработайте программу секундомер. Секундомер должен иметь три кнопки Старт, Стоп, Сбросить. 
Секундомер должен выводить время в формате 00:00:00 добавьте стили используя JS*/

let sec = 0, //секунды
min = 0, //минуты
hours = 0, //часы
time, //счётчик
get = id => document.getElementById(id); //функция поиска элемента по id 

counter = () => { // стрелочная функция,
    sec++;
    if (sec < 10) {
        get("s").innerHTML = `0${sec}`; //функция поиска элемента по id 
    } 
    if (10 <= sec && sec <= 59) {
        get("s").innerHTML = `${sec}`; //функция поиска элемента по id 
    }
    if (sec == 60){
        sec = 0;
        get("s").innerHTML = `0${sec}`; //функция поиска элемента по id 
        min++;
    } 
    if (min < 10) {
        get("m").innerHTML = `0${min} :`; //поиск элемента по id "m", добавляем текст
    }    
    if (10 <= min && min <= 59) {
        get("m").innerHTML = `${min} :`; //функция поиска элемента по id 
    }
    if (min == 60){
        min = 0;
        get("m").innerHTML = `0${min} :`; //функция поиска элемента по id 
        hours++;
    } 
    if (hours < 10) {
        get("h").innerHTML = `0${hours} :`; //функция поиска элемента по id 
    }    
    if (10 <= hours && hours <= 59) {
        get("h").innerHTML = `${hours} :`; //функция поиска элемента по id 
    }
    if (hours == 60){
        hours = 0;
        get("h").innerHTML = `0${hours} :`; //функция поиска элемента по id 
    }
}      

get("start").onclick = () => {
    time = setInterval(counter, 1000); // запуск секундомера (вызывается функция counter)
}   

get("stop").onclick = () => {
    clearInterval(time);  // остановка секундомера (остановка функции counter)
}

get("reset").onclick = () => {
    sec = -1; // присваивание значение sec 0 (сбрасываем секунды на 0)
    min = 0; // присваивание значение min 0 (сбрасываем минуты на 0)
    hours = 0; // присваивание значение hours 0 (сбрасываем часы на 0)
    counter(); // запуск секундомера (вызывается функция counter) c 00:00:00
}
