window.onload = () => {
    const body = document.querySelector("body"); // находим body и добавляем ему стили
    body.style.minWidth = "800px";
    body.style.maxWidth = "1600px";
   
    const input = document.querySelector("input"); // находим кнопку и добавляем ей стили
    input.style.width = "100px";
    input.style.height = "100px";
    input.style.borderRadius = "50%";
    input.style.boxShadow = "1px 1px 10px darkslategrey";
    input.style.backgroundColor = "red";
    input.style.color = "white";
    input.style.fontSize = "10px";
    input.style.fontWeight = "bold";
    input.style.margin = "20px";
        
    const button = document.querySelector("input"); // находим элемент по имени тега (кнопка)
    button.onclick = () => { // событие нажатия на кнопку
        const buttonDelete = document.querySelector("input"); // находим элемент по имени тега (кнопка)
        buttonDelete.parentNode.removeChild(buttonDelete); // удаляем кнопку
                
        let diameter = () => { // создаём функцию определения диаметра
            let pattern = /\d/; // регулярное выражение, которое допускает только цифры
            userNumber = prompt("Введите диаметр круга", "Целое число от 1 до ..."); //запрашиваем диаметр у пользователя
            d = pattern.test(userNumber); // проверяем, ввёл ли пользователь число
            if (d !== true) { // если пользователь ввел не чило
                alert("Это не число. Введите пожалуйста число"); // просим ввести число
                diameter();// снова вызываем функцию для запроса числа от пользователя
            }
            return userNumber;// функция возвращает число, которое ввёл пользователю 
        } 
        
        let circleDiameter = (diameter()); // вызываем функцию, и сохраняем сюда ее значение

        const conteiner = document.createElement("div"); // создаем тег div, куда будем вставлять круги
        document.body.append(conteiner); // добавляем тег div на страницу
        for (let i = 0; i < 10; i++) { //запускаем цикл создания кругов 10 х 10
            for (let j = 0; j < 10; j++) {
                const circle = document.createElement("div"); // создаем тег div для рисования кргуа
                circle.className = "cirlce"; // 
                circle.style.border = "1px solid black"; // рамка для просмотра где рисунок
                let r = Math.floor(Math.random() * (256)), // рандомно определеям цвет по rgb
                g = Math.floor(Math.random() * (256)),// рандомно определеям цвет по rgb
                b = Math.floor(Math.random() * (256)),// рандомно определеям цвет по rgb
                randomColor = '#' + r.toString(16) + g.toString(16) + b.toString(16); // присваиваем полный цвет по rgb
                circle.style.backgroundColor = randomColor; // добавляем цвет коругу
                circle.style.width = circleDiameter + "px"; // добавляем ширину
                circle.style.height = circleDiameter + "px";// добавляем висоту
                circle.style.borderRadius = "50%";// делаем круг
                circle.style.display = "inline-block";
                conteiner.append(circle); // добавляем тег div на страницу
            }
            const br = document.createElement("br");
            conteiner.append(br); // добавляем перенос строки
        }
        
        conteiner.onclick = function(event) { // событие нажатия на круг, всплытие через родителя
            event.target.style.display = "none"; // временно удаляеm элемент из документа
            conteiner.style.display = "block";
        }  
    }
}