window.onload = () => { 
    const bttnNext = document.querySelector("#next"), // ищем элемент по id, присваиваем значение переменной
    bttnPrev = document.querySelector("#prev"), //ищем элемент по id, присваиваем значение переменной
    slides = document.querySelectorAll("img"), //ищем элемент по имени тега, присваиваем значение переменной
    dots = document.querySelectorAll(".dot"); //ищем элемент по классу, присваиваем значение переменной
    let startSlide = 1; // стартовий слайд


    show = (step) => { // функция слайдера
        if (step > slides.length) { // переход с последнего слайда на первый
            startSlide = 1; //
        }
        if (step < 1) { // переход с первого слайда на последний
            startSlide = slides.length; //
        }
        for (let i = 0; i < slides.length; i++) { // запускаем счетчик
            slides[i].style.display = "none"; // временно удаляем элемент (не отображается)
        }
        for (let i = 0; i < dots.length; i++) { // запускаем счетчик
            dots[i].className = dots[i].className.replace(" active", " "); //временно удаляем класс(active) и его свойства

        }
        slides[startSlide - 1].style.display = "block"; // добавляем элемент (отображается)
        dots[startSlide - 1].className += " active"; // добавляем элементу класс(active) и его свойства
    }
    
    nextSlide = () => { // 
        show(startSlide += 1); // вызов функции слайдера с шагом вперед
    }

    previewSlide = () => { //
        show(startSlide -= 1); //вызов функции слайдера с шагом назад
    }

    dotsSlide = (index) => { //
        show (startSlide = index); // //вызов функции слайдера для точек
    }

    bttnNext.onclick = () => { // событие нажатия на кнопку bttnNext
        nextSlide(); // вызов функции nextSlide
    }

    bttnPrev.onclick = () => { // событие нажатия на кнопку bttnPrev
       previewSlide (); // вызов функции previewSlide
    }
    let addEvents = () => {
        for(let i = 0; i < dots.length; i++) {  
            dots[i].onclick = () => { dotsSlide(i+1); }
        }
    }
    addEvents(dots);
}