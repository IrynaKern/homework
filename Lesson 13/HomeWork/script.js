window.onload = function () {
    const loader = document.querySelector("#loader"); // находим элемент по id, в котором хранится gif
    
    function show() { // функция для отображения индикатора загрузки
        loader.style.display = "block";
    }
    
    function hide() {// функция для удаления индикатора загрузки.
        loader.style.display = "none";
    }

    let xhr = new XMLHttpRequest(); 

    document.querySelector("#btn").onclick = function () { //настройка объекта запроса с указание метода отправи запроса и данных
        xhr.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
        
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                hide(); // скрыть индикатор
                let data = JSON.parse(xhr.responseText), //переобразование полученной с сервера строки в объект
                filtered = data.filter(value => value.rate >=25); // филтруем данные - курс валюты больше 25грн.
                const table = document.querySelector("table"), // находим элемент по имени тега
                tbody = document.querySelector("tbody");// находим элемент по имени тега
                table.style.display = "block"; //делаем видимой таблицу
                filtered.forEach((element) => { // создаём функцию перебора массива для каждого элемента
                    tr = document.createElement("tr");// создаём переменную и присваиваем ей значение - создаём элемент (тег) tr
                    for (let key in element) {// создаём функцию перебора свойств объекта
                        const td = document.createElement("td"); //создаём переменную и присваиваем ей значение - создаём элемент (тег) td
                        td.innerHTML = `${element[key]}`; // добавляем текст в td
                        tr.appendChild(td); // добавляем элемент (тег) td в tr
                    }
                tbody.appendChild(tr); // добавляем элемент (тег) tr в tbody 
                })
            } 
            if (xhr.status !== 200) { // если статус НЕ 200
                hide(); // скрыть индикатор
                const msg = document.createElement("p"); // создать элемент 
                msg.innerHTML = "Виникла помилка - " + xhr.status + ". Спробуйте пізніше.";// добавить текст с указанием кода ошибки
                document.body.append(msg);// добавить эелемент в документ
            }
        }
        
        xhr.send();
        show(); // отобразить индикатор.
    }

}