/*     ЗАДАНИЕ
Реализовать функцию фильтра массива по указанному типу данных.
Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. 
Первый аргумент - массив, который будет содержать в себе любые данные, 
второй аргумент - тип данных. 
Функция должна вернуть новый массив, который будет содержать в себе все данные, 
которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. 
То есть, если передать массив ['hello', 'world', 23, '23', null], 
и вторым аргументом передать 'string', то функция вернет массив [23, null].*/


// создаём переменную, которой присваимваем массив с разным типом данных
const myArr = ["javascript", 100, "48", ["hello", "world", 78], "html", 52, 88, "java"], 

// создаём функцию, которая будет принимать в себя 2 аргумента: 1й - массив, 2й - тип данных. 
filterBy = (arr, typeofdata) => { 
      for (let i = 0; i < arr.length; i++) { // включаем цикл
        if (typeof arr[i] !== typeofdata) { // условие, что элемент НЕ наш тип данных
            newArr.push(arr[i]); // добавляем элемент в новый массив 
        }
    }
}

const newArr = []; // создаём переменную, которой присваимваем пустой массив
filterBy(myArr, "number"); // вызов функции фильтра массива по указанному типу данных

console.log(newArr); // выводим результат в консоль
