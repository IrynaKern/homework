/*Реализовать функцию для создания объекта "пользователь".
Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
Создать пользователя с помощью функции createNewUser().
Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.*/

class CreateNewUser { // создаем класс (ES6), который создает и возвращает объект newUser
    constructor(name, surName) { // создаем конструктор (ES6), который создает и возвращает объект newUser со свойствми и значениями
        let firstName = prompt("What is your first name?"); // Спрашиваем у пользователя имя
        let lastName = prompt("What is your last name?"); // Спрашиваем у пользователя фамилию
        this.firstName = firstName;  // Присваиваем значения свойствам объекта (newUser)
        this.lastName = lastName; // Присваиваем значения свойствам объекта (newUser)
    }
    getLogin() { // Создаем метод в объект (newUser)
        let firstСharFirstName = this.firstName.substring(0, 1).toLowerCase(), // первая буква имени пользователя в нижнем регистре
        lowerCaselastName = this.lastName.toLowerCase(), // фамилия пользователя в нижнем регистре
        user = firstСharFirstName + lowerCaselastName;  // объединяем первую букву имени и фамилию пользователя в нижнем регистре
        console.log(user); // выводим в консоль результат выполнения функции
    }
}

const newUser = new CreateNewUser("", ""); // создаем объект "пользователь"

console.log(newUser); // выводим в консоль результат создания пользователя (newUser)

newUser.getLogin(); // вызов у пользователя (newUser) функции getLogin()

