/*Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
Возьмите выполненное задание выше (созданная вами функция createNewUser()) 
и дополните ее следующим функционалом: 
При вызове функция должна спросить у вызывающего дату рождения 
(текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
 Создать метод getAge() который будет возвращать сколько пользователю лет. 
 Создать метод getPassword(), который будет 
 возвращать первую букву имени пользователя в верхнем регистре, 
 соединенную с фамилией (в нижнем регистре) 
 и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992). 
 Вывести в консоль результат работы функции createNewUser(), 
 а также функций getAge() и getPassword() созданного объекта.*/

class CreateNewUser { // создаем класс (ES6), который создает и возвращает объект newUser
    constructor(name, surName, age) { // создаем конструктор (ES6), который создает и возвращает объект newUser со свойствми и значениями
        let firstName = prompt("What is your first name?"), // Спрашиваем у пользователя имя
        lastName = prompt("What is your last name?"), // Спрашиваем у пользователя фамилию
        birthday = prompt("When is your birthday?", "dd.mm.yyyy"); // Спрашиваем у пользователя дату рождения в формате dd.mm.yyyy
        this.firstName = firstName; // Присваиваем значения свойствам объекта (newUser)
        this.lastName = lastName; // Присваиваем значения свойствам объекта (newUser)
        this.birthday = birthday; // Присваиваем значения свойствам объекта (newUser)
    }

    getLogin() { // Создаем метод в объект (newUser)
        let firstСharFirstName = this.firstName.substring(0, 1).toLowerCase(), // первая буква имени пользователя в нижнем регистре
        lowerCaselastName = this.lastName.toLowerCase(), // фамилия пользователя в нижнем регистре
        user = firstСharFirstName + lowerCaselastName;  // объединяем первую букву имени и фамилию пользователя в нижнем регистре
        console.log(user); // выводим в консоль результат выполнения функции
    }
    getAge() {
        let arr = this.birthday.split("."), // содание массива чисел из введённой даты рождения
        today = new Date(), // текущя дата
        dayOfBorn = new Date(arr[2], arr[1]-1, arr[0]), // ДР в формате yyyy.dd.mm
        dayOfBornNow = new Date(today.getFullYear(), dayOfBorn.getMonth(), dayOfBorn.getDate()), // ДР в текущем году
        age = today.getFullYear() - dayOfBorn.getFullYear(); // dозраст = текущий год - год рождения
            if (today < dayOfBornNow) {//Если ДР в этом году ещё предстоит, то вычитаем из age один год
                age = age-1;
            }
        console.log(`${this.firstName}, you are ${age} years old.`); // выводим в консоль результат выполнения функции
    }

    getPassword() {
        let firstСharFirstName = this.firstName.substring(0, 1).toUpperCase(), // первая буква имени пользователя в верхнем регистре
        lowerCaselastName = this.lastName.toLowerCase(), // фамилия пользователя в нижнем регистре
        arr = this.birthday.split("."), // содание массива чисел из введённой даты рождения
        user = firstСharFirstName + lowerCaselastName + arr[2];  // объединяем первую букву имени (верх.регистр) и фамилию пользователя (нижний регистр) и год рождения
    console.log(user); // выводим в консоль результат выполнения функции
    }
}

const newUser = new CreateNewUser("", ""); // создаем объект "пользователь"
console.log(newUser); // выводим в консоль результат создания пользователя (newUser)

newUser.getLogin(); // вызов у пользователя (newUser) функции getLogin()
newUser.getAge(); // вызов у пользователя (newUser) функции getAge()
newUser.getPassword(); // вызов у пользователя (newUser) функции getPassword()
