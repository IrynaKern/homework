window.addEventListener('DOMContentLoaded', () => {
    const bannerMove = () => {
        const banner = document.querySelector("#banner"); //поиск элемента по id banner, сохраняем в переменную
        const bannerRun = () => {
            const min = 1;// минимальное значение 
            max = 800;// максимальное значение 
            let bannerX = Math.floor(Math.random()* (max - min) + min);// рандомное число в интервале
            bannerY = Math.floor(Math.random()*(max - min) + min);// рандомное число в интервале
            banner.style.left = bannerX + "px";// плавающее позиционирование баннера по Х
            banner.style.top = bannerY + "px";//плавающее позиционирование баннера по Y
            banner.style.zIndex = "10"; //позиционирование баннера (главнее всех)
        }
        banner.addEventListener("mouseover", bannerRun); // навешиваем обработчик, который "слушает" событие bannerRun и запускает функцию обработчик
    }
    bannerMove()// вызов функции для убегающего баннера

    const formRadio = document.forms[0], // поиск первой формы в документе, сохраняем в переменную
    pizzaSize = document.querySelector("#size"), // поиск элемента по id size, сохраняем в переменную
    priceTotal = document.querySelector("#sum"),// поиск элемента по id price, сохраняем в переменную
    saucesAdd = document.querySelector("#sauces"),// поиск элемента по id sauces, сохраняем в переменную
    topingsAdd = document.querySelector("#toppings");// поиск элемента по id topings, сохраняем в переменную
    let priceBase = 100, // цена за размер большой пицци по умолчанию
    priceSelectedIngridients = 0, // цена всех добавленных ингридиентов 
    ingridient, // объявляем переменную, будем записывать перетаскиваемый элемент
    myPizzaIngridients = [];// массив для хранения ингридиентов, выбранных пользователем
    priceTotal.innerHTML = priceBase + priceSelectedIngridients;

    formRadio.onclick = () => { //обработчик события нажатия на форму
        if (!formRadio[1].checked && !formRadio[2].checked) {// проверяем что НЕ выбраны второй и третий
            formRadio[0].checked = true;// значит выбран первый
            pizzaSize.style.width = "75%";// меняем размер пицци на первый (маленького размера)
            priceBase = 60;// устанавливаем цену пицци маленького размера
        }
        if (!formRadio[0].checked && !formRadio[2].checked) {// проверяем что НЕ выбраны первый и третий
            formRadio[1].checked = true;// значит выбран второй
            pizzaSize.style.width = "85%";// меняем размер пицци на второй (среднего размера)
            priceBase = 80;// устанавливаем цену пицци среднего размера
        }
        if (!formRadio[0].checked && !formRadio[1].checked) {// проверяем что НЕ выбраны второй и первый
            formRadio[2].checked = true;// значит выбран третий
            pizzaSize.style.width = "95%";// меняем размер пицци на третий (большого размера)
            priceBase = 100;// устанавливаем цену пицци большого размера
        }

        myPizzaIngridients.forEach((myPizzaIngridient) => { // перебор кажого элеиента массива
        if (myPizzaIngridient.style.width != pizzaSize.style.width) {//для визуальной коррекции размера ингридиента
                myPizzaIngridient.style.width = pizzaSize.style.width; // под размер пицци
            }
        });

    priceTotal.innerHTML = priceBase + priceSelectedIngridients; // отображаем цену выбранной пицци + " грн."
        
    }
   
    const dragAndDrop = () => {
        const draggableImages = document.querySelectorAll(".draggable"), // поиск элемента по классу, сохраняем в переменную
        base = document.querySelector(".base");// поиск элемента по классу, сохраняем в переменную
          
        const dragStart = function () { //функция - начало переноса
            ingridient = this.cloneNode(); // клонирует перетаскиваемый элемент
        };
      
        const dragOver = function(e) {
            e.preventDefault();//функция, отменяет события dragover браузера
        };

        const dragDrop = function() { //функция - положить элемент 
            ingridient.style.position = "absolute"; //устанавливаем стиль - позиционироавние
            ingridient.style.width = pizzaSize.style.width; //устанавливаем стиль - ширину
            this.append(ingridient); // добавляем элемент в место для перетаскивания (тесто пицци)
            myPizzaIngridients.push(ingridient);//добавляем каждый ингридиент в массив ингридиентов
            let nameOfIingridient = ingridient.getAttribute("alt");//переменная, в которой хранится имя (значение атрибута alt) ингридиента
            if (ingridient.classList.contains("sauces")) {//если элемент содержит класс sauces
                saucesAdd.innerHTML+= nameOfIingridient + "<br>";// отображаем значение атрибута внутри тега

            } 
            if (ingridient.classList.contains("toppings")) { //если элемент содержит другой toppings
                topingsAdd.innerHTML+= nameOfIingridient + "<br>";// отображаем значение атрибута внутри тега
                
            };

            let ingPrice = parseInt(ingridient.dataset.price);// достаем данные из собственного атрибута и переводим его в число
            priceSelectedIngridients += ingPrice; //добаляем цену каждого ингридиента
            priceTotal.innerHTML = priceBase + priceSelectedIngridients;//отображаем цену на странице
        };
            
        draggableImages.forEach((draggableImage) => { // перебираем массив и на каждый элемент навешиваем обработчик
            draggableImage.addEventListener("dragstart", dragStart)// который "слушает" событие dragstart и запускает функцию обработчик
        });

        base.addEventListener("dragover", dragOver); // навешиваем обработчик, который "слушает" событие dragover и запускает функцию обработчик
        base.addEventListener("drop", dragDrop);// навешиваем обработчик, который "слушает" событие drop и запускает функцию обработчик
    
    }

    dragAndDrop(); // вызов функции
})   