$(function(){
    
    let slideShow = function() {
        let activeSlide = $(".slide.current");
        activeSlideIndex = $(".slide.current").index();
        nextActiveSlideIndex = activeSlideIndex + 1;
        nextActiveSlide = $(".slide").eq(nextActiveSlideIndex);
        activeSlide.removeClass("current");
 
        if(nextActiveSlideIndex == ($(".slide:last").index()+1)){
            $(".slide").eq(0).addClass("current");
         } else {
            nextActiveSlide.addClass("current");
         }
    };
    
    $(function() {
       setInterval(slideShow, 15000);
     });

    $("#next").click(function() {
       let currentSlide = $(".slide.current"),
       currentSlideIndex = $(".slide.current").index(),
       nextSlideIndex = currentSlideIndex + 1,
       nextSlide = $(".slide").eq(nextSlideIndex);
       currentSlide.removeClass("current");

       if(nextSlideIndex == ($(".slide:last").index()+1)){
           $(".slide").eq(0).addClass("current");
        } else {
            nextSlide.addClass("current");
        }
    });

    $("#prev").click(function() {
    let currentSlide = $(".slide.current"),
    currentSlideIndex = $(".slide.current").index(),
    prevSlideIndex = currentSlideIndex - 1,
    prevSlide = $(".slide").eq(prevSlideIndex);
    currentSlide.removeClass("current");
    prevSlide.addClass("current");
   });
}); 